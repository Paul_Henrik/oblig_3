#include "PriorityQueue.h"

PriorityQueue::PriorityQueue()
{
    itemCount = 0;
}

void PriorityQueue::insert(string elem, int priority)
{
    Node *newNode = createNode(elem, priority);
    int pos = itemCount - 1;
    heap.push_back(newNode);
    while(true)
    {
        if (pos == 0)   //is at root
            break;

        int parent = (pos-1)/2;
        if (heap.at(pos)->priority >= heap.at(parent)->priority)
            break;
        else
        {
            Node* temp = heap.at(pos);
            heap.at(pos) = heap.at(parent);
            heap.at(parent) = temp;
            pos = parent;
        }
    }

}

void PriorityQueue::remove()
{
    if (itemCount > 0)
    {
        string removed = heap.at(0)->elem;
        int priorityRemove = heap.at(0)->priority;
        heap.at(0) = heap.at(itemCount - 1);
        itemCount--;
        heap.pop_back();

        int pos = 0;

        //heapify();

        while (true)
        {
            int childLeft  = 2*pos + 1;
            int childRight = 2*pos + 2;

            if(childLeft >= itemCount) //out of queue
                break;

            int smallestChild;

            if(childRight >= itemCount)     //No second child
            {
                smallestChild = childLeft;
            }
            else if (heap.at(childLeft)->priority <= heap.at(childRight)->priority)  //
            {
                smallestChild = childLeft;
            }
            else
            {
                smallestChild = childRight;
            }

            if(heap.at(smallestChild)->priority >= heap.at(pos)->priority)
            {
                break;
            }
            Node* temp;
            temp = heap.at(pos);
            heap.at(pos) = heap.at(smallestChild);
            heap.at(smallestChild) = temp;

            pos = smallestChild;
        }
        cout << endl  << removed << " was removed from the Queue! Priority: " << priorityRemove << endl;
    }
}


Node *PriorityQueue::createNode(string elem, int priority)
{
    Node* n = new Node;

    n->elem = elem;
    n->priority = priority;
    n->position = itemCount;
    itemCount++;
    return n;
}

void PriorityQueue::printAll()
{
    for (vector<Node*>::const_iterator it = heap.begin(), end = heap.end();it != end; ++it)
        cout << (*it)->elem << " " << (*it)->priority << endl;
}

