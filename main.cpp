#include <iostream>
#include "PriorityQueue.h"
#include <chrono>
#include <thread>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

void timer();

int addNode = 0;
int removeNode = 1000;
int wait = 0;
bool runAddNode;
string name[10] = {"Paul", "Kaya", "Steinar", "Ole", "Dag", "Rebecca", "Andre", "Adrian", "Berd", "Andreas"};

int main()
{
    PriorityQueue pq;
    pq.insert("Askeladde", 1);
    pq.insert("Peer", 3);
    pq.insert("Paal", 8);
//    pq.insert("Ola", 1);
//    pq.insert("Nordmann", 22);
//    pq.insert("Kari",21);
//    pq.insert("NordKvinne", 25);
//    pq.insert("Jenni", 26);
    srand(time(NULL));
    while(true)
    {
        if (addNode <= 0)
        {
            addNode = rand()%3000 + 1;
        }
        if (removeNode <= 0)
            removeNode = 1000;

        if (addNode < removeNode)
        {
            wait = addNode;
            removeNode -= addNode;
            runAddNode = true;
            addNode = 0;
        }
        else
        {
            wait = removeNode;
            addNode -= removeNode;
            removeNode = 0;
            runAddNode = false;
        }

        auto start = std::chrono::high_resolution_clock::now();
        std::this_thread::sleep_for(std::chrono::milliseconds(wait));
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end-start;

        if (runAddNode)
        {
            pq.insert(name[rand()%10], rand()%20+1);
        }
        if (!runAddNode)
        {
            if(pq.size() > 0)
            {
                cout << "Time: " << elapsed.count() << endl;
                pq.remove();
                pq.printAll();
            }
        }
    }
    pq.printAll();
    return 0;
}

/*
    Clock:
    std::chrono::system_clock:              current time current to the systems clock.
    std::chrono::steady_clock:              goes at a uniform rate
    std::chrono::high_resolution_clock      provides smalles possible tick period.

    std::chrono::duration<>:                represents time duration
        2 hours ( a number and a unit)
    duration<int, ratio<1,1>>               number of seconds stores in a int
    duration<double, ratio<60,1>>           number of minutes stored in a double
        nanoseconds, microsecons, milliseconds, seconds, minutes, hours     //predefined durations in Chrono

    system_clock::duration -- duration<T, system_clock::period>
    time_point<system_clock, milliseconds: according to system_clock, the elapsed time since epoch in milliseconds

    /////////////////////////////////////EXAMPELS/////////////////////////////////////////////////////////////////
    std::ratio<1, 10> r; // A ratio of 1/10
    cout << r.num << "/" << r.den << endl;

    cout << chrono::system_clock::period::num << "/" << chrono::system_clock::period::den << endl;

    chrono::microseconds  mi(2700);       //
    mi.count();
    chrono::nanoseconds na = mi;        //
    na.count();
    chrono::milliseconds mill = chrono::duration_cast<chrono::milliseconds>(mi);     // 2 milliseconds

    mi = mill + mi; // 2000 + 2700 = 4700

    /////////////////////
    chrono::steady_clock::time_point start = chrono::steady_clock::now();
    cout << "I am bored" << endl;
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    chrono::steady_clock::duration d = end - start;
    if(d == chrono::steady_clock::duration::zero())
        cout << "no time elapsed";
    cout << chrono::duration_cast<chrono::microseconds>(d).count() << endl;

    ////////////////chrono::steady_clock::time_point start = chrono::steady_clock::now();
    chrono::steady_clock::time_point end;
    cout << "I am bored" << endl;
    chrono::seconds oneSecond(1);

    oneSecond + oneSecond;
    //if ()
    cout << chrono::duration_cast<chrono::milliseconds>(oneSecond).count() << endl;
    chrono::steady_clock::time_point end = chrono::steady_clock::now();
    chrono::steady_clock::duration d = end - start;
    if(d == chrono::steady_clock::duration::zero())
        cout << "no time elapsed";
    cout << chrono::duration_cast<chrono::microseconds>(d).count() << endl;
*/
