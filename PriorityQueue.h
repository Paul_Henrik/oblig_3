#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H
#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct Node
{
    string elem;
    int priority;
    int position;
};

class PriorityQueue
{
public:
    PriorityQueue();
    void insert(string elem, int priority);
    void remove();
    int size() {return itemCount;}
    Node *createNode(string elem, int priority);
    void printAll();
private:
    vector<Node*> heap;
    int itemCount;
};





#endif // PRIORITYQUEUE_H
